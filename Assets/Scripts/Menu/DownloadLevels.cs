﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class DownloadLevels : MonoBehaviour {

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void SyncFiles();

    [DllImport("__Internal")]
    private static extern void WindowAlert(string message);
#endif

    private string link = "http://5.9.113.235:8081/";
    string dowbloadxml = "http://5.9.113.235:8081/manifest.xml";

    public GameObject tt;
    Text text;

    UpdateManifest oldUp;
    UpdateManifest newUp;

    string oldMani;
    string newMani;
    
    // Use this for initialization
    void Start () {
        text = tt.GetComponent<Text>();
        oldMani = Application.persistentDataPath + "/Scripts/manifest.xml"; ;
        newMani = Application.persistentDataPath + "/Scripts/manifestNew.xml";
    }

    public void getLevels()
    {
        text.text = "";

        StartCoroutine(downloadManifest(dowbloadxml));
    }

    IEnumerator getLevelFromOutside(string BaseURL, string filename)
    {
        WWW www = new WWW(BaseURL + filename);
        //Continue after this statement, when the download is yield finished
        yield return www;

        string savePath = Application.persistentDataPath + "/Scripts/Levels/" + filename;

        if (!Directory.Exists(Application.persistentDataPath + "/Scripts/Levels/"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Scripts/Levels/");
        }
        
        byte[] bytes = www.bytes;
        text.text += "New file added: " + filename + "\n";
        File.WriteAllBytes(savePath, bytes);

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {

#if UNITY_WEBGL
            SyncFiles();
#endif
        }
    }

    IEnumerator downloadManifest(string url)
    {
        WWW www = new WWW(url);
        //Continue after this statement, when the download is yield finished
        yield return www;

        string savePath = newMani;

        if (!Directory.Exists(Application.persistentDataPath + "/Scripts/"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Scripts/");
        }

        string bytes = www.text;
        text.text += "Level werden verglichen...\n";
        File.WriteAllText(savePath, bytes);

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
#if UNITY_WEBGL
            SyncFiles();
#endif
        }

        CompareManifest(newMani, oldMani);
    }

    public void CompareManifest(string newManifest, string oldManifest)
    {
        if (!File.Exists(oldManifest))
        {
            ReadNewManifest(newManifest);

            File.Move(newManifest, oldManifest);

            foreach(LevelHolder h in newUp.Levels)
            {
                StartCoroutine(getLevelFromOutside(link, "Level_" + h.id + ".json")); //Alle Level neu herunterladen
            }
        }else{
            ReadOldManifest(oldManifest);
            ReadNewManifest(newManifest);

            if (oldUp.Levels.Length == newUp.Levels.Length)
            {
                foreach(LevelHolder h in oldUp.Levels)
                {
                    foreach(LevelHolder i in newUp.Levels)
                    {
                        if (h.id == i.id)
                        {
                            if (h.version < i.version || h.version > i.version) //Sollte mal ein Fehler beim version bumb passieren usw.
                            {
                                StartCoroutine(getLevelFromOutside(link, "Level_" + i.id + ".json")); //Get new Level version
                            }
                        }
                    }
                }
            }

            if(oldUp.Levels.Length < newUp.Levels.Length)
            {
                bool same = false;
                foreach (LevelHolder h in newUp.Levels)
                {
                    foreach (LevelHolder i in oldUp.Levels)
                    {
                        if (h.id == i.id)
                        {
                            if (h.version < i.version || h.version > i.version) //Sollte mal ein Fehler beim version bumb passieren usw.
                            {
                                StartCoroutine(getLevelFromOutside(link, "Level_" + i.id + ".json")); //Get new Level version
                            }

                            same = true;
                        }
                    }

                    if(same == false)
                    {
                        StartCoroutine(getLevelFromOutside(link, "Level_" + h.id + ".json")); //Get new Level
                    }

                    same = false;
                }
            }

            if (oldUp.Levels.Length > newUp.Levels.Length)
            {
                bool same = false;

                foreach (LevelHolder h in oldUp.Levels)
                {
                    foreach (LevelHolder i in newUp.Levels)
                    {
                        if (h.id == i.id)
                        {
                            if (h.version < i.version || h.version > i.version) //Sollte mal ein Fehler beim version bumb passieren usw.
                            {
                                StartCoroutine(getLevelFromOutside(link, "Level_" + i.id + ".json")); //Get new Level version
                            }

                            same = true;
                        }
                    }

                    if (same == false)
                    {
                        File.Delete(Application.persistentDataPath + "/Scripts/Levels/Level_" + h.id + ".json"); //Delete Level
                    }

                    same = false;
                }
            }
            File.Delete(oldManifest);
            File.Move(newManifest, oldManifest);
        }

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {

#if UNITY_WEBGL
            SyncFiles();
#endif
        }

        text.text += "Alle Level aktualisiert\n";
    }

    public void ReadOldManifest(string filename)
    {
        string ff = Application.persistentDataPath + "/Scripts/manifest.xml";

        XmlSerializer serializer = new XmlSerializer(typeof(UpdateManifest));

        serializer.UnknownNode += new
        XmlNodeEventHandler(serializer_UnknownNode);
        serializer.UnknownAttribute += new
        XmlAttributeEventHandler(serializer_UnknownAttribute);

        // A FileStream is needed to read the XML document.
        FileStream fs = new FileStream(ff, FileMode.Open); //ersetzten mit filename methoden parameter
        oldUp = (UpdateManifest) serializer.Deserialize(fs);

        fs.Close();
    }

    public void ReadNewManifest(string filename)
    {
        string ff = Application.persistentDataPath + "/Scripts/manifestNew.xml";

        XmlSerializer serializer = new XmlSerializer(typeof(UpdateManifest));

        serializer.UnknownNode += new
        XmlNodeEventHandler(serializer_UnknownNode);
        serializer.UnknownAttribute += new
        XmlAttributeEventHandler(serializer_UnknownAttribute);

        // A FileStream is needed to read the XML document.
        FileStream fs = new FileStream(ff, FileMode.Open); //ersetzten mit filename methoden parameter
        newUp = (UpdateManifest)serializer.Deserialize(fs);

        fs.Close();
    }

    private void serializer_UnknownNode
    (object sender, XmlNodeEventArgs e)
    {
        print("Unknown Node:" + e.Name + "\t" + e.Text);
    }

    private void serializer_UnknownAttribute
    (object sender, XmlAttributeEventArgs e)
    {
        System.Xml.XmlAttribute attr = e.Attr;
        print("Unknown attribute " +
        attr.Name + "='" + attr.Value + "'");
    }
}

[XmlRoot("UpdateManifest", IsNullable = false)]
public class UpdateManifest
{
    [XmlArray("Levels")]
    public LevelHolder[] Levels { get; set; }

    public int ManifestVersion;
}

[XmlType(TypeName = "Level")]
public class LevelHolder
{
    [XmlAttribute]
    public int id;
    [XmlAttribute]
    public int version;
    [XmlElement]
    public string Description;
}