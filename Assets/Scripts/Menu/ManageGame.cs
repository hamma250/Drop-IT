﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManageGame : MonoBehaviour {

    public GameObject tt;
    Text text;
    
    public GameObject PanelMain;
    public GameObject PanelOptions;
    public GameObject PanelScores;

    private RectTransform PanelMainRectTrans;
    private RectTransform PanelOptionsRectTrans;
    private RectTransform PanelScoresRectTrans;

    public GameObject Slider;
    private Slider slider;

    public GameObject ExitButton;
    private Button Exit_but;
    
    private int ScreenWidth = Screen.width;
    private int goOut;

    //Versioning
    public static string GAME_VERSION_NAME = "Alpha";
    public static string GAME_VERSION_STRING = "0.0.3";
    public GameObject VersionText;
    private Text versionText;

    //Redundant
    public GameObject ScoreTextG;
    Text ScoreText;

    //Data access
    DataAccessLayer DataAcc = new DataAccessLayer(null);

    //bool ready = false; //Variable für den manifest download und Levelvergleich im Hintergrund. Das Spiel kann erst gestartet werden, nachdem diese Variable auf true gesetzt wird. Ganz am Anfang müssen die Level alle 24 Stunden gecheckt und geupdated wewrden wenn nötig.

    // Use this for initialization
    void Start ()
    {
        goOut = ScreenWidth * 100;

        if (PlayerPrefs.GetInt("First Run", 0) == 0)
        {
            PlayerPrefs.SetInt("Tilt Speed", 400);
            PlayerPrefs.SetInt("First Run", 1);
        }

        text = tt.GetComponent<Text>();

        PanelMainRectTrans = PanelMain.GetComponent<RectTransform>();
        PanelOptionsRectTrans = PanelOptions.GetComponent<RectTransform>();
        PanelScoresRectTrans = PanelScores.GetComponent<RectTransform>();

        slider = Slider.GetComponent<Slider>();

        ScoreText = ScoreTextG.GetComponent<Text>();

        PanelMainRectTrans.anchoredPosition = new Vector2(0f, 0f);
        PanelOptionsRectTrans.anchoredPosition = new Vector2(goOut, 0f);
        PanelScoresRectTrans.anchoredPosition = new Vector2(goOut, 0f);

        Exit_but = ExitButton.GetComponent<Button>();

        //Den Exit Button für nicht unterstütze Platformen deaktivieren
        if(Application.platform != RuntimePlatform.Android || Application.platform != RuntimePlatform.LinuxPlayer || Application.platform != RuntimePlatform.WindowsPlayer || Application.platform != RuntimePlatform.OSXPlayer)
        {
            Exit_but.enabled = false;
            ExitButton.SetActive(false);
        }

        //Versioning
        versionText = VersionText.GetComponent<Text>();
        versionText.text = GAME_VERSION_NAME + " " + GAME_VERSION_STRING;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void StartGame(string SceneName)
    {
        //Nur starten, wenn die Level_x.json Dateien da sind und der Download fertig ist; Bei keinem Internet nur prüfen, ob mindestens ein Level da ist und anzeigen wieviele Level es gibt
        string[] FileLevelPaths = Directory.GetFiles(Application.persistentDataPath + "/Scripts/Levels/");
        
        if(FileLevelPaths.Length > 0)
        {
            SceneManager.LoadScene(SceneName);
        }

        if(FileLevelPaths.Length <= 0)
        {
            text.text = "";
            text.text = "Please download the Levels";
        }
    }

    public void ExitGame()
    {
        //Für android. Bei iOS sollte man dies nicht machen. Siehe: https://developer.apple.com/library/content/qa/qa1561/_index.html
        Application.Quit();
    }

    public void OpenIssues()
    {
        Application.OpenURL("https://github.com/hamma2/droppin-issues");
    }

    public void OpenOptions()
    {
        PanelMainRectTrans.anchoredPosition = new Vector2(goOut, 0f);
        PanelOptionsRectTrans.anchoredPosition = new Vector2(0f, 0f);

        slider.value = DataAcc.LoadTiltValue();
    }

    public void CloseOptions()
    {
        PanelMainRectTrans.anchoredPosition = new Vector2(0f, 0f);
        PanelOptionsRectTrans.anchoredPosition = new Vector2(goOut, 0f);

        DataAcc.SaveTiltValue(slider.value);
    }

    public void toHighscores()
    {
        //Load Scores
        List<float> scores = DataAcc.Load();
        //Text entfernen von vorigen aufrufen
        ScoreText.text = "";

        if (scores == null)
        {
            ScoreText.text = "Spiele dein erstes Spiel\n";
        }
        else
        {
            foreach (float i in scores)
            {
                if (i > 0)
                {
                    ScoreText.text = ScoreText.text + i.ToString() + "\n";
                }
            }
        }

        PanelMainRectTrans.anchoredPosition = new Vector2(goOut, 0f);
        PanelScoresRectTrans.anchoredPosition = new Vector2(0f, 0f);
    }

    public void backFromHighscores()
    {
        PanelScoresRectTrans.anchoredPosition = new Vector2(goOut, 0f);
        PanelMainRectTrans.anchoredPosition = new Vector2(0f, 0f);
    }
}
