﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreObject : BaseExtra
{
    public int amount;
    public string add_or_multiply;
    public float multiplay_dauer;

    public GameObject texter;
    public Canvas can;

    public Sprite plus10;
    public Sprite plus20;
    public Sprite plus50;
    public Sprite plus100;

    public Sprite mul2;
    public Sprite mul4;
    public Sprite mul10;

    void Start()
    {
        if (amount > 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else if (amount < 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }

        if(add_or_multiply == "+")
        {
            if(amount == 10)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = plus10;
            }
            if (amount == 20)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = plus20;
            }
            if (amount == 50)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = plus50;
            }
            if (amount == 100)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = plus100;
            }

        }

        if (add_or_multiply == "x")
        {
            if(amount == 2)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = mul2;
            }
            if (amount == 4)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = mul4;
            }
            if (amount == 10)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = mul10;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            if (ex != null)
            {
                if(add_or_multiply == "+")
                {
                    ex.AddScoreObject(amount);
                }else if(add_or_multiply == "x")
                {
                    ex.AddScoreMultiply(amount, multiplay_dauer);
                }
                else
                {
                    print("wrong sign");
                }

                if (amount > 0)
                {
                    ex.PlaceEmitter(transform.position, Color.green);
                }
                else if (amount < 0)
                {
                    ex.PlaceEmitter(transform.position, Color.red);
                }

                GameObject teee = Instantiate(texter, transform.position, Quaternion.identity);
                teee.transform.SetParent(can.transform, false);

                teee.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

                teee.GetComponent<TextPupUp>().printScorePop(add_or_multiply + " " + Mathf.FloorToInt(amount).ToString(), 0.5f);

                Destroy(this.gameObject);
            }
        }

        if (col.gameObject.name == "RightCollider")
        {
            moveDir = -1;
        }

        if (col.gameObject.name == "LeftCollider")
        {
            moveDir = 1;
        }
    }
}
