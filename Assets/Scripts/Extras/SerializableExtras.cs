﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializableExtras{

    //Level Eigentschaften und Daten
    public string LevelName;
    public string BackgroundColor;
    public string RectColor;
    public int LevelDauer; //Anzahl an Balken, die durchlaufen werden muss
    public float Speed;
    public int LevelNumber; //Diese darf nicht zweimal vorkommen
    public string Schwierigkeit_x; //Je nachdem wir die Werte sind spawnen. Das wird so ein range Bereich zwischen 0 und 100% bei Viewport Screen. Enthält eine Range als float array.
    public string Schwierigkeit_y; //Je nachdem wir die Werte sind spawnen. Das wird so ein range Bereich zwischen 0 und 100% bei Viewport Screen. Enthält eine Range als float array.
    public float point_multilier = 1; //Jetzt auch in den Extras definiert: Fixes #34

    public int LevelVersion = 0; //Muss nach jeder Änderung erhöht werden (Man kann sie auch verringern sollte man mal was verkackt haben und der bumb total unsinnig sein)
    //Es muss noch die Möglichkeit geschaffen werden exakte Level zu generieren. Zum Beispiel alle RECTS positionen usw. und auch die der Items zu definieren. Bis dahin aber erstsmal per procedurale generiert.
    //Hier immer neue Extras hinzufügen
    public ScoreObjectSeri[] ScoreObjectSeri = { };
    public SteuerungsumkehrSeri[] SteuerungsumkehrSeri = { };
    public StroboskopSeri[] StroboskopSeri = { };
    public JumpingBallSeri[] JumpingBallSeri = { };
    public GravityExtraSeri[] GravityExtraSeri = { };
    public GhostExtraSeri[] GhostExtraSeri = { };
    
    public static SerializableExtras CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<SerializableExtras>(jsonString);
    }
}

/// <summary>
/// Hier sind alle Klassen, welche die Extras Serializable machen.
/// @spawnTime Nach durchlaufen dieses Balken wird gespawnt.
/// @dauer Diese Variable dient der Extradauer im Level nachdem spawnen
/// @movSpeed Diese Variable gibt die Geschwindigkeit des Extras an
/// </summary>

[Serializable]
public class ScoreObjectSeri
{
    public float spawnTime;
    public int dauer;
    public float movSpeed;
    public int amount;
    public string add_or_multiply;
    public float multiplay_dauer; //Wird einfach auf null gesetzt bei addy
}


[Serializable]
public class SteuerungsumkehrSeri
{
    public float spawnTime;
    public int dauer;
    public float movSpeed;
    public int speed;
    public int richtung; //1 oder 0
    public int steuerDauer;
}

[Serializable]
public class StroboskopSeri
{
    public float spawnTime;
    public int dauer;
    public float movSpeed;
    public float changeSpeed;
    public int strobDauer;
}

[Serializable]
public class JumpingBallSeri
{
    public float spawnTime;
    public int dauer;
    public float movSpeed;
    public float bounciness;
    public int jumpDauer;
}

[Serializable]
public class GravityExtraSeri
{
    public float spawnTime;
    public int dauer;
    public float movSpeed;
    public int dauerGravity;
    public float gravityValue;
}

[Serializable]
public class GhostExtraSeri
{
    public float spawnTime;
    public int dauer;
    public float movSpeed;
    public int useTimes;
}
