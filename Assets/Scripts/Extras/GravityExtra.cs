﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityExtra : BaseExtra
{

    public int dauerGravity = 5;
    public float gravityValue = 9.81f;

    void Start()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            if (ex != null)
            {
                ex.GravityExtra(dauerGravity, gravityValue);

                ex.PlaceEmitter(transform.position, Color.red);
                
                Destroy(this.gameObject);
            }
        }

        if (col.gameObject.name == "RightCollider")
        {
            moveDir = -1;
        }

        if (col.gameObject.name == "LeftCollider")
        {
            moveDir = 1;
        }
    }
}
