﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostExtra : BaseExtra
{
    public int useTimes = 3;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            if (ex != null)
            {
                ex.AddGhostExtra(useTimes);

                ex.PlaceEmitter(transform.position, Color.green);

                Destroy(this.gameObject);
            }
        }

        if (col.gameObject.name == "RightCollider")
        {
            moveDir = -1;
        }

        if (col.gameObject.name == "LeftCollider")
        {
            moveDir = 1;
        }
    }
}
