﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSelfDestroy : MonoBehaviour {


    ParticleSystem pp;
	// Use this for initialization
	void Start () {
        pp = this.GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!pp.isEmitting)
        {
            Destroy(this.gameObject);
        }
	}
}
