﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steuerungsumkehr : BaseExtra
{

    public int speed = 1;
    public int richtung = 1;
    public int steuerDauer = 5;

    void Start()
    {
        if(richtung == -1)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if(richtung == 1)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }
    } 

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            if (ex != null)
            {
                ex.Steuerungsumkehr(speed, richtung, steuerDauer);
                if (richtung == -1)
                {
                    ex.PlaceEmitter(transform.position, Color.red);
                }
                else if (richtung == 1)
                {
                    ex.PlaceEmitter(transform.position, Color.green);
                }
                Destroy(this.gameObject);
            }
        }

        if(col.gameObject.name == "RightCollider")
        {
            moveDir = -1;
        }

        if (col.gameObject.name == "LeftCollider")
        {
            moveDir = 1;
        }
    }
}
