﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;
using System;

public class Extras : MonoBehaviour {

    public GameObject BallPlayer;
    BallPlayer bpl;

    public GameObject Score;
    Score score;

    public GameObject emitter;

    public GameObject GeneratorGameobject;
    ProcGenerator procGen;
    ColliderSide colsSide;
    
    //Punktemulti
    public float multi = 1f;

    //Für die Camera
    Camera c;
    public Camera BackCam;

    //Für den Partikel
    Color col;

    //Für das Stroboskop
    private float timeFTW;
    private float timeFTWStrob = 0;
    private bool StrobExecBit = false;
    private int strobDauer;
    private float changeSpeed;
    
    //Player get al compnents
    CircleCollider2D cc;
    Rigidbody2D rr;

    //Steuerungsumkehr countdown
    public Text Steuerungstext_Countdown;
    private int dauerSteuerCount = 0;
    float screenXp = Screen.width / 100.0f;
    public float FontMultiplierSize = 1f;

    //GUI canvas
    public Canvas can;
    
    //Inventar
    public GameObject InventoryGameObject;
    Inventory inventory;

    //Ghost
    public Sprite GhostSprite;


    // Use this for initialization
    void Start ()
    {
        c = Camera.main;
        col = c.backgroundColor;

        bpl = BallPlayer.GetComponent<BallPlayer>();
        score = Score.GetComponent<Score>();

        cc = bpl.GetComponent<CircleCollider2D>();
        rr = bpl.GetComponent<Rigidbody2D>();

        procGen = GeneratorGameobject.GetComponent<ProcGenerator>();
        colsSide = GeneratorGameobject.GetComponent<ColliderSide>();

        inventory = InventoryGameObject.GetComponent<Inventory>();
    }
	
	// Update is called once per frame
	void Update () {
        //StroboskopExec
        if (StrobExecBit)
        {
            StroboskopExec();
        }

        if (dauerSteuerCount <= 0)
        {
            Steuerungstext_Countdown.text = "";
            Steuerungstext_Countdown.fontSize = 0;
        }

        if(dauerSteuerCount > 0)
        {
            Steuerungstext_Countdown.fontSize = Mathf.FloorToInt(screenXp * FontMultiplierSize * 1.5f); //Dynamic font size
            FontMultiplierSize += 0.1f;
        }
    }

    //////////////////////////////////////////////////////////////////////Steuerungsumkehr////////////////////////////////////////////////////////////////////////////
    public void Steuerungsumkehr(int speed, int richtung, int steuerDauer)
    {
        //Speed zwischen 1 und 200, richtung 1 ist normal, richtung -1 ist umgekehrt
        //Andere Richtungswerte beeinflussen die Beschleunigung und führen zu komischen Ergebnissen
        bpl.speed = speed;
        bpl.richtung = richtung;


        if(dauerSteuerCount <= 0)
        {
            Timing.RunCoroutine(showCountDownSt(), Segment.SlowUpdate);
        }

        dauerSteuerCount += steuerDauer;
        Steuerungstext_Countdown.text = "Attention " + dauerSteuerCount;
    }

    IEnumerator<float> showCountDownSt()
    {
        Steuerungstext_Countdown.text = "Attention " + dauerSteuerCount;
        Steuerungstext_Countdown.fontSize = 0;
        yield return Timing.WaitForSeconds(1);

        dauerSteuerCount -= 1;

        if (dauerSteuerCount > 0)
        {
            FontMultiplierSize = 1f;
            Timing.RunCoroutine(showCountDownSt(), Segment.SlowUpdate);
        }else
        {
            Steuerungstext_Countdown.text = "";
            Steuerungstext_Countdown.fontSize = 0;
            FontMultiplierSize = 1f;
            
            bpl.speed = PlayerPrefs.GetInt("Tilt Speed");

            bpl.richtung = 1;
        }
    }

   //////////////////////////////////////////////////////////////////////AddScoreObject////////////////////////////////////////////////////////////////////////////
    public void AddScoreObject(int addy)
    {
        score.score += addy;
    }

    public void AddScoreMultiply(int mul, float multiplay_dauer)
    {
        multi *= mul;
        Timing.RunCoroutine(addMulti(mul, multiplay_dauer), Segment.SlowUpdate);
    }

    IEnumerator<float> addMulti(int mul, float multiplay_dauer)
    {
        yield return Timing.WaitForSeconds(multiplay_dauer);
        multi /= mul;
    }

    //////////////////////////////////////////////////////////////////////Stroboskop////////////////////////////////////////////////////////////////////////////
    public void Stroboskop(float changeSpeed2, int strobDauer2)
    {
        changeSpeed = changeSpeed2;
        strobDauer = strobDauer2;
        StrobExecBit = true;
        timeFTW = 0;
        timeFTWStrob = 0;

        c.backgroundColor = Color.black;
        BackCam.backgroundColor = Color.black;
        ToggleCul();
    }

    private void StroboskopExec()
    {
        timeFTW += Time.deltaTime;

        if(timeFTWStrob >= changeSpeed)
        {
            ToggleCamColor();
            ToggleCul();
            timeFTWStrob = 0;
        }

        timeFTWStrob += Time.deltaTime;

        if (timeFTW >= strobDauer)
        {
            StrobExecBit = false;
            c.backgroundColor = col;
            BackCam.backgroundColor = col;
            ShowCul();
        }
    }

    // Turn on the bit using an OR operation:
    private void ShowCul()
    {
        c.cullingMask |= 1 << LayerMask.NameToLayer("Default");
        BackCam.cullingMask |= 1 << LayerMask.NameToLayer("Background Image");
    }

    // Turn off the bit using an AND operation with the complement of the shifted int:
    private void HideCul()
    {
        c.cullingMask &= ~(1 << LayerMask.NameToLayer("Default"));
        BackCam.cullingMask &= ~(1 << LayerMask.NameToLayer("Background Image"));
    }

    // Toggle the bit using a XOR operation:
    private void ToggleCul()
    {
        c.cullingMask ^= 1 << LayerMask.NameToLayer("Default");
        BackCam.cullingMask ^= 1 << LayerMask.NameToLayer("Background Image");
    }

    private void ToggleCamColor()
    {
        if (c.backgroundColor == Color.black)
        {
            c.backgroundColor = col;
        }
        else if (c.backgroundColor == col)
        {
            c.backgroundColor = Color.black;
        }

        if(BackCam.backgroundColor == Color.black)
        {
            BackCam.backgroundColor = col;
        }
        else if (BackCam.backgroundColor == col)
        {
            BackCam.backgroundColor = Color.black;
        }
    }

    //////////////////////////////////////////////////////////////////////Partikel nachdem Hit////////////////////////////////////////////////////////////////////////////
    public void PlaceEmitter(Vector3 pos, Color col)
    {
        GameObject em = Instantiate(emitter, pos, Quaternion.identity);
        ParticleSystem emer = em.GetComponent<ParticleSystem>();
        emer.startColor = col;
    }

    //////////////////////////////////////////////////////////////////////Springender Ball////////////////////////////////////////////////////////////////////////////
    public void jumpingBall(float bounciness, int jumpDauer)
    {
        rr.isKinematic = true; //Rigidbody schlafen legen
        rr.Sleep();
        cc.sharedMaterial.bounciness = bounciness;
        rr.isKinematic = false; // rigidbody wieder neuladen
        rr.WakeUp();
        
        Timing.RunCoroutine(startJumgpingBall(jumpDauer), Segment.SlowUpdate);
    }

    IEnumerator<float> startJumgpingBall(int dauer)
    {
        yield return Timing.WaitForSeconds(dauer);
        
        rr.isKinematic = true; //Rigidbody schlafen legen
        rr.Sleep();
        cc.sharedMaterial.bounciness = 0;
        rr.isKinematic = false; // rigidbody wieder neuladen
        rr.WakeUp();
    }

    //////////////////////////////////////////////////////////////////////Gravitationsumkehr////////////////////////////////////////////////////////////////////////////
    public void GravityExtra(int gravityDauer, float gravityValue)
    {
        procGen.modifyDirection = -1f;
        Physics2D.gravity = new Vector3(0, gravityValue, 0);
        colsSide.colliders["Top"].position = new Vector3(colsSide.transform.position.x, -25, colsSide.transform.position.z);
        
        Timing.RunCoroutine(startGravityExtra(gravityDauer), Segment.SlowUpdate);
    }

    IEnumerator<float> startGravityExtra(int dauer)
    {
        yield return Timing.WaitForSeconds(dauer);
        procGen.modifyDirection = 1f;
        Physics2D.gravity = new Vector3(0, -9.81f, 0);
        colsSide.colliders["Top"].position = new Vector3(colsSide.transform.position.x, 25, colsSide.transform.position.z);
    }

    //////////////////////////////////////////////////////////////////////Ghost////////////////////////////////////////////////////////////////////////////
    public void AddGhostExtra(int useTimes)
    {
        Dictionary<String, int> dd = new Dictionary<String, int>();
        dd.Add("Usable Times", useTimes);

        Item ghost = new Item(0, "ghost", GhostSprite, dd);

        inventory.addItem(ghost);
    }

    public void ExecGhost()
    {
        bpl.goGhost = true;
        bpl.ccd.enabled = true;
    }

}
