﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseExtra : MonoBehaviour {

    public GameObject Extras;
    public Extras ex;

    private int assignee = 0;
    
    float timeFTW;

    public int moveDir = 1; //Wenn man das zu 0 setzt bewegen sich die objekte nicht
    public float movSpeed = 0.2f;

    public int dauer = 1;

    void Update()
    {
        if (assignee == 0)
        {
            ex = Extras.GetComponent<Extras>();
        }

        if (ex != null)
        {
            assignee = 1;
        }

        timeFTW += Time.deltaTime;
        if (timeFTW > dauer)
        {
            Destroy(this.gameObject);
        }

        transform.position = new Vector3(transform.position.x + movSpeed * moveDir * Time.timeScale, transform.position.y, transform.position.z);
    }
}
