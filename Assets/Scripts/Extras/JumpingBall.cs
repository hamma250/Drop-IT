﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingBall : BaseExtra
{
    public float bounciness; // 0 - 1 sind gute Werte. alles was höher liegt ist echt heftig
    public int jumpDauer;

    void Start()
    {
        if (bounciness <= 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.green; // es gibt nur schlechte
        }
        else if (bounciness > 0)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            if (ex != null)
            {
                ex.jumpingBall(bounciness, jumpDauer);
                ex.PlaceEmitter(transform.position, Color.red);
                Destroy(this.gameObject);
            }
        }

        if (col.gameObject.name == "RightCollider")
        {
            moveDir = -1;
        }

        if (col.gameObject.name == "LeftCollider")
        {
            moveDir = 1;
        }
    }
}
