﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stroboskop : BaseExtra
{
    public float changeSpeed = 0.2f;
    public int strobDauer = 2;
    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            if (ex != null)
            {
                //Hier kommt der Extra Code rein
                ex.PlaceEmitter(transform.position, Color.black);
                ex.Stroboskop(changeSpeed, strobDauer);
                Destroy(this.gameObject);
            }
        }

        if (col.gameObject.name == "RightCollider")
        {
            moveDir = -1;
        }

        if (col.gameObject.name == "LeftCollider")
        {
            moveDir = 1;
        }
    }
}
