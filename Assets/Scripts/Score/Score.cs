﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    //Nur zum Test: das ganze muss noch schön werden. TextureLabel usw. und das Größen skalierien muss auf allen Bildschirmgrößen funktionieren. 
    //Dafür am Besten ein Textfield erstellen und als GameObject adden. Ist am einfachsten...
    public float score = 0;
    public Text gameScoreText;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        gameScoreText.text = "Score: " + score;
    }
}
