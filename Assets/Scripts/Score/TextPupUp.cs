﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using MEC;
using System.Collections.Generic;

public class TextPupUp : MonoBehaviour {

    Text text;

    float screenXp = Screen.width / 100.0f;

    public float FontMultiplierSize = 1f;

    public void printScorePop(string message, float delay)
    {
        Timing.RunCoroutine(ShowMessage(message, delay), Segment.SlowUpdate);
    }
	
    void Update()
    {
        if(text != null){
            text.fontSize = Mathf.FloorToInt(screenXp * FontMultiplierSize * 1.5f); //Dynamic font size
            FontMultiplierSize += 0.15f;
        }
    }

	public IEnumerator<float> ShowMessage(string message, float delay)
    {
        text = gameObject.GetComponent<Text>();
        text.fontSize = 0;
        text.text = message;
        text.enabled = true;
        yield return Timing.WaitForSeconds(delay);
        Destroy(gameObject);
    }
}
