﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDetector : MonoBehaviour {

    public GameObject Schwierig;
    Schwierig schwer;

    public GameObject texter;
    public Canvas can;
        
    // Use this for initialization
    void Start () {
        schwer = Schwierig.GetComponent<Schwierig>();
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            if ((col.gameObject.transform.position.y < this.gameObject.transform.position.y && schwer.procgen.modifyDirection > 0) || (col.gameObject.transform.position.y > this.gameObject.transform.position.y && schwer.procgen.modifyDirection < 0))
            {
                //Add Score Extra multiplier und Levelfortschritt und bei Wiederhlolung der Level um 2 erhöhter multiplier
                int scor_addy = Mathf.FloorToInt(1 * schwer.levelDesc.ex.point_multilier) * Mathf.FloorToInt(schwer.Extras.GetComponent<Extras>().multi * schwer.pointRotationMultiplier);
                schwer.score.score += scor_addy;

                /*GameObject teee = Instantiate(texter, transform.position, Quaternion.identity);
                teee.transform.SetParent(can.transform, false);

                teee.transform.position = new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z);

                teee.GetComponent<TextPupUp>().printScorePop("+ " + scor_addy, 0.5f);
                */
                schwer.BalkenCounter += 1;
                Destroy(gameObject);
            }
        }
    }
       
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "TopCollider")
        {
            Destroy(gameObject);
        }
    }
}
