﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class LevelDesc : MonoBehaviour {
    /*
     * Hier kommen die Beschreibungen der Level rein, welche von Schwierig.cs genutzt werden, um das jeweilige Level mit Extras zu füllen usw.
     * Eigenschaften sind:
     * Hintergrund
     * Extras und deren Definitionen
     * usw.
     * Hier werden außerdem alle Values für das Level gesetzt, welche nur einmalig gestzt werden müssen
     * */
    public List<int> Level_Number = new List<int>(); //Hier ein neues level adden

    //Hintergrund
    public Material BackgroundMaterial;
    private float timevar = 0f;
    Color BackColor;

    //Schwierig.cs
    public GameObject Schwierig;
    Schwierig schwierig;

    //Level Daten
    public SerializableExtras ex = null;

    //Um den Levelnamen anzuzeigen als Debug
    public int score = 0;
    GUIStyle style = new GUIStyle();

    void Start()
    {
        //Test
        float screenXp = Screen.width / 100.0f;


        style.normal.textColor = Color.black;
        style.fontSize = Mathf.FloorToInt(screenXp * 5f); //Dynamic fontsize change depending on the screen width
        
        schwierig = Schwierig.GetComponent<Schwierig>();

        //Level laden
        string ff = Application.persistentDataPath + "/Scripts/manifest.xml";

        XmlSerializer serializer = new XmlSerializer(typeof(UpdateManifest));
        
        // A FileStream is needed to read the XML document.
        FileStream fs = new FileStream(ff, FileMode.Open); //ersetzten mit filename methoden parameter
        UpdateManifest oldUp = (UpdateManifest)serializer.Deserialize(fs);

        fs.Close();

        foreach (LevelHolder h in oldUp.Levels)
        {
            Level_Number.Add(h.id);
        }

       LoadNewLevel(Level_Number[0]); //Level 1 laden
    }

    void Update()
    {
        if(BackgroundMaterial.GetFloat("_Versch") >= 0)
        {
            timevar -= Time.deltaTime;
            BackgroundMaterial.SetFloat("_Versch", timevar);

            if(timevar <= 0)
            {
                timevar = 0.75f;
                BackgroundMaterial.SetColor("_Color2", BackColor); //Top color
            }
        }
    }
    
    private void changeBackground(Color col)
    {
        BackgroundMaterial.SetColor("_Color", col); //Bottom color
        BackgroundMaterial.SetFloat("_Versch", 0.75f); // Verscheibung setzten
    }

    private SerializableExtras getNewLevel(string json)
    {
        return SerializableExtras.CreateFromJSON(json);
    }
    
    public void LoadNewLevel(int levelid)
    {

        if (File.Exists(Application.persistentDataPath + "/Scripts/Levels/Level_" + levelid + ".json"))
        {
            ex = getNewLevel(File.ReadAllText(Application.persistentDataPath + "/Scripts/Levels/Level_" + levelid + ".json"));

            //Hintergrund
            BackColor = ColorExtensions.ParseColor(ex.BackgroundColor); //Hintergrund wird hier geparsed und gesetzt
            changeBackground(BackColor);

            if (ex.LevelNumber == 1) //Am Anfang kein gradient
            {
                BackgroundMaterial.SetColor("_Color", BackColor); //Bottom color
                BackgroundMaterial.SetColor("_Color2", BackColor); //Top color
                BackgroundMaterial.SetFloat("_Versch", 0.75f); // Verscheibung setzten
            }

            //Leveldauer
            schwierig.BalkenLevelDauer = ex.LevelDauer;

            //Speed der Rects
            schwierig.changeSpeed(ex.Speed);

            //Farbe der Rects
            schwierig.procgen.RectCol = ColorExtensions.ParseColor(ex.RectColor);
            schwierig.procgen.RectColorChange = true;
        }
    }

    //Nur für Testzwecke welches Level geladen ist.
    /*void OnGUI()
    {
        GUI.Label(new Rect(0,100, 200, 100), ex.LevelName,style);
    }*/
}
