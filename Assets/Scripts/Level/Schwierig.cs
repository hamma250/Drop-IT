﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schwierig : MonoBehaviour {
    
    //Levelwechselzähler
    public int counterLevel = 0;
    
    //Leveldauerzähler
    public int BalkenCounter = 0;
    public int BalkenLevelDauer;

    //Bei nächster Rotation den multiplier um 2 erhöhen
    public float pointRotationMultiplier = 1f;

    //Level Script: Hier sind die Level Aktionen beschrieben
    public GameObject LevelDesc;
    public LevelDesc levelDesc;
    public GameObject Extras;

    //Score
    public GameObject Score;
    public Score score;

    //Positionen für die Items
    Camera c;

    //ProcGenerator, um den Speed der Rects zu verändern
    public GameObject ProcGenerator;
    public ProcGenerator procgen;

    //Hier kommen alle Extras rein, welche es gibt:
    public GameObject SteuerungsumkehrPreflab;
    public GameObject ScoreObjectPreflab;
    public GameObject StroboskopPreflab;
    public GameObject JumpingBallPreflab;
    public GameObject GravityExtraPreflab;
    
    //Für die Prozedurale Positionsbestimmung
    System.Random random = new System.Random();

    //Canvas for GUI
    public Canvas can;

    // Use this for initialization
    void Start ()
    {
        c = Camera.main;
        levelDesc = LevelDesc.GetComponent<LevelDesc>();
        score = Score.GetComponent<Score>();
        procgen = ProcGenerator.GetComponent<ProcGenerator>();
        BalkenLevelDauer = 10; //Die .json Level auslesen dauert ein wenig. Die Zeit verzögern durch setzen eines Defaults
    }
	
	// Update is called once per frame
	void Update () {
        updateTimer();
        levelChange(); //TODO: diese methode optimieren
    }

    private void updateTimer()
    {
        if (BalkenCounter >= BalkenLevelDauer)
        {
            counterLevel += 1;
            if (counterLevel < levelDesc.Level_Number.Count)
            {
                //Load new Level
                levelDesc.LoadNewLevel(levelDesc.Level_Number[counterLevel]);
            }
            else
            {
                counterLevel = 0;
                //Level von vorne beginnend wieder laden und die Geschwindigkeit der Rects und den multiplier der Punkte erhöhen.
                procgen.speed += 5;
                pointRotationMultiplier += 2;
                levelDesc.LoadNewLevel(levelDesc.Level_Number[0]);
            }
            BalkenCounter = 0;
        }
    }

    public void changeSpeed(float speed)
    {
            //procgen.speed += speed; //Hier sollte man ??? (Der Change muss noch überprüft werden nach performance optimierung) pro level nehmen; Zu Übungszwecken und bei kürzerer Leveldauer sollte diese Zahl kleiner sein. Sonst wird es zu schnell schnell xD
    }

    private void levelChange()
    {
        //Score
        for (int i = 0; i < levelDesc.ex.ScoreObjectSeri.Length; i++)
        {
            if(levelDesc.ex.ScoreObjectSeri[i].spawnTime == BalkenCounter)
            {
                float x = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[1]) / 100f;
                float y = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[1]) / 100f;

                GameObject sc1 = Instantiate(ScoreObjectPreflab, c.ViewportToWorldPoint(new Vector3(x, y, 10)), Quaternion.identity);
                ScoreObject scc = sc1.GetComponent<ScoreObject>();
                scc.Extras = Extras;
                scc.amount = levelDesc.ex.ScoreObjectSeri[i].amount;
                scc.dauer = levelDesc.ex.ScoreObjectSeri[i].dauer;
                scc.add_or_multiply = levelDesc.ex.ScoreObjectSeri[i].add_or_multiply;
                scc.multiplay_dauer = levelDesc.ex.ScoreObjectSeri[i].multiplay_dauer;
                scc.can = can;
                scc.movSpeed = levelDesc.ex.ScoreObjectSeri[i].movSpeed;

                //Sicherstellen, dass das Item nur einmal gespawnt wird: Muss ich noch irgendwie besser machen. sollte so aber erstmal gehen. 
                //TODO: überprüfen, ob das hier Sinn macht. Checke ich grade nicht was das soll. Yup Balken + Balkenanzahl die durchlaufen wird sind immer größer als Leveldauer
                levelDesc.ex.ScoreObjectSeri[i].spawnTime += levelDesc.ex.LevelDauer; //Das muss auf jeden Fall länger als das Level sein. Insofern kann dieses Item nicht nochmal im selben Level spawnen. funkioniert nicht mehr. naja
            }
        }

        //Stroboskop
        for (int i = 0; i < levelDesc.ex.StroboskopSeri.Length; i++)
        {
            if(levelDesc.ex.StroboskopSeri[i].spawnTime == BalkenCounter)
            {
                float x = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[1]) / 100f;
                float y = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[1]) / 100f;

                GameObject sb1 = Instantiate(StroboskopPreflab, c.ViewportToWorldPoint(new Vector3(x, y, 10)), Quaternion.identity);
                Stroboskop sbs1 = sb1.GetComponent<Stroboskop>();
                sbs1.Extras = Extras;
                sbs1.dauer = levelDesc.ex.StroboskopSeri[i].dauer;
                sbs1.changeSpeed = levelDesc.ex.StroboskopSeri[i].changeSpeed; //Schnelligkeit des Stroboskobeffekts
                sbs1.strobDauer = levelDesc.ex.StroboskopSeri[i].strobDauer;
                sbs1.movSpeed = levelDesc.ex.StroboskopSeri[i].movSpeed;

                levelDesc.ex.StroboskopSeri[i].spawnTime += levelDesc.ex.LevelDauer;
            }
        }

        //Steurerungsumkehr
        for (int i = 0; i < levelDesc.ex.SteuerungsumkehrSeri.Length; i++)
        {
            if(levelDesc.ex.SteuerungsumkehrSeri[i].spawnTime == BalkenCounter)
            {
                float x = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[1]) / 100f;
                float y = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[1]) / 100f;

                GameObject st1 = Instantiate(SteuerungsumkehrPreflab, c.ViewportToWorldPoint(new Vector3(x, y, 10)), Quaternion.identity);
                Steuerungsumkehr sss = st1.GetComponent<Steuerungsumkehr>();
                sss.Extras = Extras;
                sss.speed = levelDesc.ex.SteuerungsumkehrSeri[i].speed;
                sss.richtung = levelDesc.ex.SteuerungsumkehrSeri[i].richtung;
                sss.dauer = levelDesc.ex.SteuerungsumkehrSeri[i].dauer;
                sss.movSpeed = levelDesc.ex.SteuerungsumkehrSeri[i].movSpeed;

                levelDesc.ex.SteuerungsumkehrSeri[i].spawnTime += levelDesc.ex.LevelDauer;
            }
        }

        //JumpingBall
        for (int i = 0; i < levelDesc.ex.JumpingBallSeri.Length; i++)
        {
            if (levelDesc.ex.JumpingBallSeri[i].spawnTime == BalkenCounter)
            {
                float x = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[1]) / 100f;
                float y = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[1]) / 100f;

                GameObject st1 = Instantiate(JumpingBallPreflab, c.ViewportToWorldPoint(new Vector3(x, y, 10)), Quaternion.identity);
                JumpingBall sss = st1.GetComponent<JumpingBall>();
                sss.Extras = Extras;
                sss.dauer = levelDesc.ex.JumpingBallSeri[i].dauer;
                sss.bounciness = levelDesc.ex.JumpingBallSeri[i].bounciness;
                sss.jumpDauer = levelDesc.ex.JumpingBallSeri[i].jumpDauer;
                sss.movSpeed = levelDesc.ex.JumpingBallSeri[i].movSpeed;

                levelDesc.ex.JumpingBallSeri[i].spawnTime += levelDesc.ex.LevelDauer;
            }
        }

        //GravityExtra
        for (int i = 0; i < levelDesc.ex.GravityExtraSeri.Length; i++)
        {
            if (levelDesc.ex.GravityExtraSeri[i].spawnTime == BalkenCounter)
            {
                float x = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_x)[1]) / 100f;
                float y = random.Next(PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[0], PositionParserExtension.ParsePosition(levelDesc.ex.Schwierigkeit_y)[1]) / 100f;

                GameObject st1 = Instantiate(GravityExtraPreflab, c.ViewportToWorldPoint(new Vector3(x, y, 10)), Quaternion.identity);
                GravityExtra sss = st1.GetComponent<GravityExtra>();
                sss.Extras = Extras;
                sss.dauer = levelDesc.ex.GravityExtraSeri[i].dauer;
                sss.movSpeed = levelDesc.ex.GravityExtraSeri[i].movSpeed;
                sss.dauerGravity = levelDesc.ex.GravityExtraSeri[i].dauerGravity;
                sss.gravityValue = levelDesc.ex.GravityExtraSeri[i].gravityValue;

                levelDesc.ex.GravityExtraSeri[i].spawnTime += levelDesc.ex.LevelDauer;
            }
        }
    }
}
