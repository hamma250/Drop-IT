﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcGenerator : MonoBehaviour {

    Camera c;

    public int screenWidth = Screen.width;//Screen.width; //TODO: Enable those resolutions
    public int screenHeight = Screen.height;//Screen.height;

    public int LochGroeße = 20; // Teile von 100
    public int minWidth = 20; // Diese Variable ändern, um den Lochabstand untereinanderliegender Rects zu ändern
    public int maxWidth = 80; // Diese Variable ändern, um den Lochabstand untereinanderliegender Rects zu ändern
    public int minHeight = 2; //
    public int maxHeight = 5;

    public int RectAnzahl = 6; //Pro Höhe sind es tatsächlich 2*RectAnzahl, einmal links und einmal rechts und dazwischen die Lücke:  RECT ------ RECT!
                               // Dise Variable wird für die Einstellung der abstände zwischen den Balken verwendet

    private double width = 0f;

    //Rect
    public GameObject preflab;
    public GameObject Ball;

    //Keeping track of Rects
    private float sy = 0f;
    private float trackAbstand = 0f;
    public float step; //Used in MoveRect.cs, um den Balken zu bewegen.
    public float modifyDirection = 1f;
    RectMove rectMove; //RectMove.cs auf den Rects wird dieses GameObject mitgegeben
    SpriteRenderer rend1;
    GameObject n1;

    //Moving Rects
    public float speed = 5f;

    //Rect SpriteRenderer COlor
    public Color RectCol;
    public bool RectColorChange = true;

    //Ball Detektor Preflab
    public GameObject BallDetectorPreflab;

    ColliderSide colSide;
    float actupos;

    System.Random random = new System.Random();
    // Use this for initialization
    void Start () {
        colSide = this.GetComponent<ColliderSide>();

        RectCol = new Color(255, 160, 0, 255)/255; //Die Rects müssen gleich am start Farbe annehmen. Das Level wird aber erst später geladen. Hier also die Startfarbe: TODO: Das muss noch geändert werden.
        c = Camera.main;
        sy = 1 / (float)RectAnzahl; //TODO: Am Anfang muss immer 1/3 der Höhe Platz sein zum Start

        if(screenWidth > Screen.height)
        {
            screenWidth = 600;
        }

        width = Camera.main.orthographicSize * 2.0 * screenWidth / Screen.height; //Für Breite der RECTS

        actupos = c.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
        //for (int i = 0; i < RectAnzahl+3; i++) //Das letzte nicht hinzufügen... Das wäre an der Decke und sieht scheiße aus
        // {
        //      GenerateRect(nsy, 1f);
        //        nsy = nsy + 0.2f;
        //   }
    }

    //Hier kommt fast der gesamte Code aus der FixedUpdate() Methode rein, um die Perfomance zu verbessern => helps NOT. Verdammt
    void Update()
    {
        step = speed * Time.deltaTime * modifyDirection;

        //Gravitation ist nach unten gerichtet also positiv :P
        if (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y < 0.5 && c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y > 0.2 && step > 0)
        {
            step = step + step * (1 - (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y + 0.5f)) * 10;
        }

        if (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y < 0.2 && step > 0)
        {
            step = step + step * (1 - (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y + 0.5f)) * 15;
        }

        //Gravitation ist nach oben gerichtet also negativ :P
        if (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y > 0.5 && c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y < 0.8 && step < 0)
        {
            step = step - step * (1 - (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y + 0.5f)) * 10;
        }

        if (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y > 0.8 && step < 0)
        {
            step = step - step * (1 - (c.WorldToViewportPoint(new Vector3(0f, Ball.transform.position.y, 0f)).y + 0.5f)) * 15;
        }

        if(modifyDirection > 0)
        {
            trackAbstand = trackAbstand + step;
        }else
        {
            trackAbstand = trackAbstand - step;
        }

        if (0.5 - c.WorldToViewportPoint(new Vector3(0f, trackAbstand, 0f)).y <= -sy && modifyDirection > 0)
        {
            GenerateRect(-0.5f, 1f);
            trackAbstand = 0f;
        }

        if (0.5 + c.WorldToViewportPoint(new Vector3(0f, trackAbstand, 0f)).y >= 1+sy && modifyDirection < 0)
        {
            GenerateRect(1.2f, 1f);
            trackAbstand = 0f;
        }
    }

    void GenerateRect(float nsy, float Lochabstand)
    {
        nsy = c.ViewportToWorldPoint(new Vector3(0f, nsy, 10)).y;
        //Erstes Rect in der Zeile
        float x = 0f;
        float w = random.Next(minWidth, maxWidth) / 100f;
        float h = minHeight / 100f; //Höhe noch einstellen. Funktioniert noch nicht. konstant lassen?
        addNewRect(x, nsy, w, h);

        //Ball Detektor als Collider mit Trigger
        addBallColDect(w * screenWidth, nsy, LochGroeße / 100f , h);

        //Zweites Rect in der Zeile
        float x2 = (w + LochGroeße / 100f) * screenWidth;
        float w2 = 1 - LochGroeße / 100f - w;
        addNewRect(x2, nsy, w2, h);
    }

    void addBallColDect(float x, float y, float w, float h)
    {
        Vector3 pos = c.ScreenToWorldPoint(new Vector3(x, y, 10));
        float ww = (float)width * w;

        n1 = Instantiate(BallDetectorPreflab, new Vector3(colSide.colliders["Left"].position.x + colSide.colliders["Left"].localScale.x * 0.5f + pos.x - actupos, y , pos.z), Quaternion.identity);
        BoxCollider2D b2d = n1.GetComponent<BoxCollider2D>();
        
        b2d.size = new Vector2(ww, 0.4f);
        b2d.offset = new Vector2(ww/2,0.2f);

        rectMove = n1.GetComponent<RectMove>();
        rectMove.ProcGen = this.gameObject;
    }

    void addNewRect(float x, float y, float w, float h)
    {
        Vector3 pos = c.ScreenToWorldPoint(new Vector3(x, y, 10));
        float ww = (float)width * w;
        
        n1 = Instantiate(preflab, new Vector3(colSide.colliders["Left"].position.x + colSide.colliders["Left"].localScale.x * 0.5f + pos.x - actupos, y, pos.z), Quaternion.identity);

        rend1 = n1.GetComponent<SpriteRenderer>();
            rend1.size = new Vector2(ww, 0.4f);
            rend1.color = RectCol;

            rectMove = n1.GetComponent<RectMove>();
            rectMove.ProcGen = this.gameObject;

        RectColorChange = false;
    }
}
