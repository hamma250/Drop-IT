﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectMove : MonoBehaviour
{

    public GameObject ProcGen;
    public ProcGenerator gen; //ProcGenerator.cs
    private SpriteRenderer sp;
    private Transform t;

    // Use this for initialization
    void Start()
    {
        gen = ProcGen.GetComponent<ProcGenerator>();
        sp = this.gameObject.GetComponent<SpriteRenderer>();
        t = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(t.position.x, t.position.y + gen.step, t.position.z);

        t = this.transform;

        if (gen.RectColorChange && sp)
        {
            sp.color = gen.RectCol;
        }
    }
}
