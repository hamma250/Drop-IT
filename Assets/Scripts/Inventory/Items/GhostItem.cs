﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostItem : MonoBehaviour
{
    private TouchClickSprite tcp;

    // Use this for initialization
    void Start()
    {
        tcp = this.GetComponent<TouchClickSprite>();
    }

    // Update is called once per frame
    void Update()
    {
        if (tcp.change)
            ExecTouch();
    }

    void ExecTouch()
    {
        tcp.change = false;

        int value = tcp.inv.getItem(tcp.index).descDic["Usable Times"];
        
        if (value > 0)
        {
            value--;
            tcp.inv.itemText.text = value.ToString();
            tcp.inv.getItem(tcp.index).descDic["Usable Times"] = value;
            tcp.exexe.ExecGhost();
        }

        if (value <= 0)
        {
            tcp.inv.removeItem(tcp.index);
            Destroy(tcp.inv.itemText.gameObject);
            Destroy(gameObject);
        }
    }
}
