﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    private List<Item> ItemList = new List<Item>();
    public GameObject ex;
    public Extras exexe;
    public GameObject itemPreflab;
    public GameObject itemTextGO;
    public Text itemText;
    public Transform canvas_transform;

    private void Start()
    {
        exexe = ex.GetComponent<Extras>();
        drawInventory();
    }

    public void addItem(Item _item)
    {
        ItemList.Add(_item);
        drawInventory();
    }

    public void removeItem(Item _item)
    {
        ItemList.Remove(_item);
    }

    public void removeItem(int index)
    {
        ItemList.RemoveAt(index);
    }

    public Item getItem(int _index)
    {
        return ItemList[_index];
    }

    //called to draw and Update the Inventory to the GUI
    public void drawInventory()
    {
        foreach (Item item in ItemList)
        {
            //Anpassen der spawn höhe nach Anzahl der Items
            Vector3 placement = new Vector3(0.9f, 0.6f, 0f);
            GameObject gg = GameObject.Instantiate(itemPreflab, Camera.main.ViewportToWorldPoint(placement), Quaternion.identity);
            //z=0 da man die Sprite sonst nicht sieht.
            gg.transform.position = new Vector3(gg.transform.position.x, gg.transform.position.y, 0f);

            //TODO: Alles cachen
            gg.GetComponent<SpriteRenderer>().sprite = item.Sprite;
            gg.GetComponent<TouchClickSprite>().itemID = item.Id;
            gg.GetComponent<TouchClickSprite>().inv = this.GetComponent<Inventory>();
            gg.GetComponent<TouchClickSprite>().index = ItemList.IndexOf(item);
            gg.GetComponent<TouchClickSprite>().exexe = ex.GetComponent<Extras>();

            //Wenn man das Item mehrmals benutzen kann, dann auch noch den Text hinzufügen
            if (item.descDic.ContainsKey("Usable Times"))
            {
                GameObject iText = GameObject.Instantiate(itemTextGO, placement, Quaternion.identity, canvas_transform);
                RectTransform trans = iText.GetComponent<RectTransform>();
                Vector3 cc = Camera.main.ViewportToScreenPoint(placement);
                // trans.anchoredPosition = new Vector2(cc.x + gg.GetComponent<SpriteRenderer>().bounds.extents.x, cc.y - gg.GetComponent<SpriteRenderer>().bounds.extents.x); // Zum anpassen an die Sprite. funktioniert nicht
                trans.anchoredPosition = new Vector2(cc.x, cc.y);
                itemText = iText.GetComponent<Text>();
                itemText.text = item.descDic["Usable Times"].ToString();
            }

            //Jetzt das Script adden für Ghost und andere Inventory Items
            if (item.Name == "ghost")
            {
                gg.AddComponent<GhostItem>();
            }
        }
    }
}

public class Item
{
    /*ID
     * Ghost = 0
     * 
     * */
    public int Id { get; set; }
    public String Name { get; set; }
    public Sprite Sprite { get; set; }
    public Dictionary<String, int> descDic { get; set; }

    public Item(int _id, string _Name, Sprite _sprite, Dictionary<String, int> _desc)
    {
        Id = _id;
        Name = _Name;
        Sprite = _sprite;
        descDic = _desc;
    }

    ~Item() { }
}

/*Um einen GUI Text zu kreiren. https://gamedev.stackexchange.com/questions/116177/how-to-dynamically-create-an-ui-text-object-in-unity-5
 * 
 * GameObject CreateText(Transform canvas_transform, float x, float y, string text_to_print, int font_size, Color text_color)
{
    GameObject UItextGO = new GameObject("Text2");
    UItextGO.transform.SetParent(canvas_transform);

    RectTransform trans = UItextGO.AddComponent<RectTransform>();
    trans.anchoredPosition = new Vector2(x, y);

    Text text = UItextGO.AddComponent<Text>();
    text.text = text_to_print;
    text.fontSize = font_size;
    text.color = text_color;

    return UItextGO;
}
*/
