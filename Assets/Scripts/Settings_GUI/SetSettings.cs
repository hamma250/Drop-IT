﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SetSettings : MonoBehaviour {

    public GameObject PanelPause;
    public GameObject PanelSetInGame;
    public GameObject Player;
    public GameObject AfterGamePanel;
    public GameObject OptionsPanel;

    private RectTransform PanelPauseRectTrans;
    private RectTransform PanelSetInGameRectTransform;
    private RectTransform AfterGamePanelRectTrans;
    private RectTransform OptionsPanelRectTrans;

    private BallPlayer ballPlayer;

    public GameObject Slider;
    private Slider slider;
    
    DataAccessLayer DataAcc = new DataAccessLayer(null);

    private int goOut;

    // Only called once after everything else is loaded
    void Awake () {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Start()
    {
        PanelPauseRectTrans = PanelPause.GetComponent<RectTransform>();
        PanelSetInGameRectTransform = PanelSetInGame.GetComponent<RectTransform>();
        AfterGamePanelRectTrans = AfterGamePanel.GetComponent<RectTransform>();
        OptionsPanelRectTrans = OptionsPanel.GetComponent<RectTransform>();

        ballPlayer = Player.GetComponent<BallPlayer>();

        slider = Slider.GetComponent<Slider>();

        goOut = Screen.width*100;
    }

    public void PauseGame()
    {
        PanelPauseRectTrans.anchoredPosition = new Vector2(goOut, 0f);
        PanelSetInGameRectTransform.anchoredPosition = new Vector2(0f, 0f);

        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        PanelPauseRectTrans.anchoredPosition = new Vector2(0f, 0f);
        PanelSetInGameRectTransform.anchoredPosition = new Vector2(goOut, 0f);

        Time.timeScale = 1f;
    }

    public void GodModeToggle()
    {
        if(ballPlayer)
            ballPlayer.GodModeTest = !ballPlayer.GodModeTest;
    }
    
    public void Menu()
    {
        Time.timeScale = 1f; //Ansonsten stoppt das level
        Physics2D.gravity = new Vector3(0, -9.81f, 0); //Wenn Gravity noch umgekehrt war
        SceneManager.LoadScene("menu");
    }

    public void restartLevel()
    {
        Time.timeScale = 1f; //Ansonsten stoppt das level
        Physics2D.gravity = new Vector3(0, -9.81f, 0); //Wenn Gravity noch umgekehrt war
        SceneManager.LoadScene("main"); //Restart
    }

    public void SetEndGameOverlay()
    {
        PanelPauseRectTrans.anchoredPosition = new Vector2(goOut, 0f);
        AfterGamePanelRectTrans.anchoredPosition = new Vector2(0f, 0f);
    }

    public void getBackFromOptionsToEndGameOverlay()
    {
        AfterGamePanelRectTrans.anchoredPosition = new Vector2(0f, 0f);
        OptionsPanelRectTrans.anchoredPosition = new Vector2(goOut, 0f);

        DataAcc.SaveTiltValue(slider.value);
    }

    public void getFromEndGameOverlayToOptions()
    {
        AfterGamePanelRectTrans.anchoredPosition = new Vector2(goOut, 0f);
        OptionsPanelRectTrans.anchoredPosition = new Vector2(0f, 0f);

        slider.value = DataAcc.LoadTiltValue();
    }
}
