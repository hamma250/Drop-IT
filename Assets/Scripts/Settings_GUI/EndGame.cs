﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    public GameObject score;
    Score sc;

    public GameObject HighscoreTextPreflab;
    Text HighscoreText;

    public GameObject ScoreTextG;
    Text ScoreText;

    SetSettings setter;

    DataAccessLayer DataAcc;

    void Start()
    {
        sc = score.GetComponent<Score>();

        DataAcc = new DataAccessLayer(sc);

        HighscoreText = HighscoreTextPreflab.GetComponent<Text>();

        ScoreText = ScoreTextG.GetComponent<Text>();

        setter = this.GetComponent<SetSettings>();
    }

    public void endGame()
    {
        Time.timeScale = 0f;

        List<float> score = DataAcc.Load();

        //Null beim erstmaligen laden der Datei, da noch nicht geschrieben. Erst danach speichern ist einfacher dne ersten Score, welcher immer ein Highscore ist zu checken. Siehe erster Teil der Save funtion.
        //Es wären sonst der aktuelle Score und der gespeicherte und geladene gleich ;)
        if (score == null)
        {
            HighscoreText.text = "New Highscore";
            ScoreText.fontSize = 150;
        }
        else
        {
            foreach (float i in score)
            {

                //Aufgrund von der absteigenden Sortierung ist der erste Wert immer der Highscore. Also ist alles danach geringer als der neue Wert. 
                //Vielleicht noch ein ranking zwischen neuem Score in der Liste in den Top 3?
                if (sc.score > i)
                {
                    HighscoreText.text = "New Highscore";
                    ScoreText.fontSize = 150;
                    break;
                }

                HighscoreText.text = "Score";
                ScoreText.fontSize = 120;
                break;
            }
        }

        DataAcc.Save();

        ScoreText.text = sc.score.ToString();
        setter.SetEndGameOverlay();
    }
}


[Serializable]
class ScoreSeri
{
    public List<float> score; //Kann auch int sein zurzeit: Performance
}