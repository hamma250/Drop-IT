﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchClickSprite : MonoBehaviour
{

    public int itemID;
    public Inventory inv;
    public int index;
    public bool change;
    public Extras exexe;

    public GameObject emitter;

    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                // Construct a ray from the current touch coordinates
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                // Create a particle if hit
                if (Physics.Raycast(ray))
                {
                    // Touch zurücksenden
                    //change wird in der nächsten Frame von *Item.cs gecheckt und dann wird die Methode aufgerufen und change auf false gesetzt.
                    change = true;
                    PlaceEmitter(this.gameObject.transform.position);
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero);
            // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
            if (hitInfo)
            {
                //change wird in der nächsten Frame von *Item.cs gecheckt und dann wird die Methode aufgerufen und change auf false gesetzt.
                change = true;
                PlaceEmitter(this.gameObject.transform.position);
            }
        }
    }

    public void PlaceEmitter(Vector3 pos)
    {
        Instantiate(emitter, pos, Quaternion.identity);
    }
}
