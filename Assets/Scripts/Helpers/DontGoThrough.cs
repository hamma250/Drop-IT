﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontGoThrough : MonoBehaviour {

    private float minimumExtent;
    private float partialExtent;
    private float sqrMinimumExtent;
    private Vector2 previousPosition;
    public float skinWidth = 0.1f; //probably doesn't need to be changed 
    CircleCollider2D myCollider;
    private Rigidbody2D myRigidbody;
    public GameObject ProcGen;
    ProcGenerator gen;

    // Use this for initialization
    void Start () {
        myCollider = this.GetComponent<CircleCollider2D>();
        myRigidbody = this.GetComponent<Rigidbody2D>();
        gen = ProcGen.GetComponent<ProcGenerator>();
        previousPosition = myRigidbody.position;
        minimumExtent = Mathf.Min(myCollider.bounds.extents.x, myCollider.bounds.extents.y);
        partialExtent = minimumExtent * (1.0f - skinWidth);
        sqrMinimumExtent = minimumExtent * minimumExtent;
    }

    //Editor -> Project Settings -> Physics2D -> Default Contact Offset -> Höher ist unrealistischer aber er detected dann die collision.
    void FixedUpdate()
    {
        //have we moved more than our minimum extent? 
        Vector2 movementThisStep = myRigidbody.position - previousPosition;
        float movementSqrMagnitude = movementThisStep.sqrMagnitude;

        if (movementSqrMagnitude > sqrMinimumExtent*0.2)
        {
            float movementMagnitude = Mathf.Sqrt(movementSqrMagnitude);
            RaycastHit2D hitInfo = Physics2D.Raycast(previousPosition, movementThisStep, movementMagnitude + gen.step); //Von oben nach unten


            //check for obstructions we might have missed 
            if (hitInfo && hitInfo.distance > 0)
            {
                if (!hitInfo.collider)
                    return;

                if (hitInfo.collider.isTrigger)
                    //hitInfo.collider.SendMessage("OnTriggerEnter", myCollider);

                if (!hitInfo.collider.isTrigger)
                {
                    //print("Reset Position due to missing a collider down" + Time.time + "lol: " + hitInfo.distance);
                    myRigidbody.position = hitInfo.point - (movementThisStep / movementMagnitude) * partialExtent + new Vector2(0f, gen.step);
                    //print("Pos down: " + myRigidbody.position);
                }
            }
        }
        previousPosition = myRigidbody.position;
    }
}
