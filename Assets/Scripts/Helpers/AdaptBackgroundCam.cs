﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptBackgroundCam : MonoBehaviour {

    private Camera bacCam;

	// Use this for initialization
	void Start () {
        bacCam = this.GetComponent<Camera>();
        //siehe https://blogs.unity3d.com/2015/06/19/pixel-perfect-2d/
        //Meine Referenz ist ein Bildschirm mit der Höhe von 1920 Pixeln. Die Formel lautet: 192 = Höhe/ (OrthograpischeGröße * 2) im unteren Falle umgeformt
        //Damit füllt der Hintergrund immer den gesamten Screen aus.
        bacCam.orthographicSize = Screen.height / (192 * 2);
    }
}
