﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataAccessLayer {

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void SyncFiles();

    [DllImport("__Internal")]
    private static extern void WindowAlert(string message);
#endif
    private Score sc;

    /***
     * Score _sc Bennötigt für die Save Funktion der Highscores
     ***/
    public DataAccessLayer(Score _sc)
    {
        this.sc = _sc;
    }
    
    public void Save()
    {
        if(sc != null)
        {
            if (!File.Exists(Application.persistentDataPath + "/playerHighscores.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create(Application.persistentDataPath + "/playerHighscores.dat");

                ScoreSeri scSeri = new ScoreSeri();
                scSeri.score = new List<float>();

                scSeri.score.Add(sc.score);
                scSeri.score.Add(0);

                scSeri.score.Add(0);

                scSeri.score.Add(0);

                scSeri.score.Add(0);

                scSeri.score.Add(0);

                scSeri.score.Add(0);

                scSeri.score.Add(0);

                scSeri.score.Add(0);

                scSeri.score.Add(0);

                bf.Serialize(file, scSeri);
                file.Close();
            }
            else
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/playerHighscores.dat", FileMode.Open);

                ScoreSeri scSeri = (ScoreSeri)bf.Deserialize(file);
                file.Close();

                List<float> newtoWrite = scSeri.score;


                newtoWrite.Add(sc.score);

                newtoWrite.Sort((a, b) => -1 * a.CompareTo(b)); // descending sort
                newtoWrite.RemoveRange(10, newtoWrite.Count - 10);

                file = File.OpenWrite(Application.persistentDataPath + "/playerHighscores.dat");

                scSeri.score = newtoWrite;
                bf.Serialize(file, scSeri);
                file.Close();
            }

            //Data persistence in UnityWebGLBrowser siehe http://amalgamatelabs.com/Blog/4/data_persistence
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
#if UNITY_WEBGL
            SyncFiles();
#endif
            }
        }
        else
        {
            Debug.Log("Cannot save a file without data parameter");
        }
    }

    public List<float> Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerHighscores.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerHighscores.dat", FileMode.Open);

            ScoreSeri scSeri = (ScoreSeri)bf.Deserialize(file);
            file.Close();

            return scSeri.score;
        }
        return null;
    }

    public float LoadTiltValue()
    {
        return PlayerPrefs.GetInt("Tilt Speed");
    }

    public void SaveTiltValue(float value)
    {
        PlayerPrefs.SetInt("Tilt Speed", (int)value);
    }
}
