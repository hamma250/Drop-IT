﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRect : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "TopCollider")
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name == "TopCollider")
        {
            Destroy(gameObject);
        }
    }
}
