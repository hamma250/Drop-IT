﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PositionParserExtension {

    public static int[] ParsePosition(string aCol)
    {
        var strings = aCol.Split(',');
        int[] output = { 0, 0 };
        for (var i = 0; i < strings.Length; i++)
        {
            output[i] = int.Parse(strings[i]);
        }
        return output;
    }
}