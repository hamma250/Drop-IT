﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerColGhost : MonoBehaviour {

    public GameObject BallPP;
    BallPlayer bpl;

    private void Start()
    {
        bpl = BallPP.GetComponent<BallPlayer>();
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.name == "Rect(Clone)" && bpl.goGhost)
        {
            bpl.goGhost = false;
            this.GetComponent<CircleCollider2D>().enabled = false;
            Physics2D.IgnoreCollision(col, bpl.GetComponent<CircleCollider2D>(), true);
        }
    }
}
