﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallPlayer : MonoBehaviour {

    public float speed;
    public int richtung = 1;

    public bool GodModeTest = false;

    public GameObject EndGameSettings;
    EndGame eg;

    private Rigidbody2D rb;

    private float moveHorizontal;
    private Vector3 movement;

    //Ghost Extra
    public GameObject colTrig;
    public CircleCollider2D ccd;

    //Für das Ghost extra
    public bool goGhost = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        eg = EndGameSettings.GetComponent<EndGame>();

        ccd = colTrig.GetComponent<CircleCollider2D>();

        speed = PlayerPrefs.GetInt("Tilt Speed");
    }

    void Update()
    {
        moveHorizontal = Input.GetAxis("Horizontal"); //For PC
        movement = new Vector3(moveHorizontal * richtung, 0.0f, 0.0f);  //For PC
        //float moveVertical = Input.GetAxis("Vertical");

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            movement = new Vector3(Input.acceleration.x * richtung, 0.0f, 0.0f); //For android
        }

        
    }

    void FixedUpdate()
    {
        rb.AddForce(movement * speed);
    }

    public void OnValueChanged(float newValue)
    {
        speed = newValue;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "TopCollider" && !GodModeTest)
        {
            //Destroy(gameObject);
            this.GetComponent<CircleCollider2D>().sharedMaterial.bounciness = 0;
            this.GetComponent<CircleCollider2D>().sharedMaterial.friction = 0;
            
            eg.endGame();
        }
    }
}
